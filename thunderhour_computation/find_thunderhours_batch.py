import os,sys, time
from subprocess import Popen, PIPE
import enipy3 as lx

startTimeStamp = '20140101T000000'
endTimeStamp   = '20210101T000000'
# startTimeStamp = '20190602T000000'
# endTimeStamp   = '20190629T000000'
numThreads     = 32
threadWait     = 10

epoch,nano    = lx.timestamp2epochnano(startTimeStamp)
endEpoch,nano = lx.timestamp2epochnano(endTimeStamp)

inputCommands = []
while epoch < endEpoch:
    timeStamp = lx.epoch2timestamp(epoch)
    inputCommands.append( ['python3', 'find_thunderhours_hdf5.py', timeStamp ] )
    epoch += 24*60*60

#load up the threads
threads  = {}
for i in range(numThreads):
    cmd = inputCommands.pop(0)
    p = Popen( cmd )
    threads[i]  = p
    #the threads start out with a disc read, I want to wait a little before I start the next one.  
    time.sleep( threadWait )

#the main loop
while len( inputCommands ) > 0:
    # print 'top of loop', len( inFiles )
    for i in threads:
        if threads[i].poll() is not None:
            if len(inputCommands) <= 0:
                break
            print ('thread %i finished'%(i))

            cmd = inputCommands.pop(0)
            p = Popen( cmd )
            threads[i] = p
            time.sleep(threadWait)

    time.sleep(60)

#loop through the threads and wait for them all to close
while len(threads) > 0:
    for i in list( threads.keys() ):
        if threads[i].poll() is not None:
            print ('thread %i finished'%(i))
            #this thread is done
            threads.pop(i)
    time.sleep(1)