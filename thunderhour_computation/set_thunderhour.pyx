import numpy as np
cimport numpy as np
ctypedef np.uint8_t uint8
from libc.math cimport sin, cos, pi, sqrt

cdef float BINSIZE = 0.05  #this is the resolution of the thunderhour array
cdef float BBOX00 =  -90.0
cdef float BBOX01 =   90.0
cdef float BBOX10 = -180.0
cdef float BBOX11 =  180.0


cdef dist( float lat1, float lon1, float lat2, float lon2 ):
    '''
    dist is based on enipy3.pythagorean_distance
    This is a pythagorean approximation of the oblate spheroid distance 
    between two lat,lon points on earth.  

    The approximation give accurate results out to distances of 100-200 km
    Computation time for this function is very fast
    
    returns: distance (meters)
    '''
    
    #copy and convert to radians
    lat1 = lat1*pi/180
    lat2 = lat2*pi/180
    lon1 = lon1*pi/180
    lon2 = lon2*pi/180  

    cdef float F = (lat1+lat2)/2
    #these are the trig functions we need
    cdef float cosf = cos(F)
    cdef float sinf = sin(F)
    #these are trig identities, multiple angle formula
    #i'm being particular and avoiding power functions, because that's slower
    cdef float cos2f = cosf*cosf - sinf*sinf
    cdef float cos3f = cosf*cosf*cosf - 3*cosf*sinf*sinf
    cdef float cos4f = cosf*cosf*cosf*cosf - 6*cosf*cosf*sinf*sinf + sinf*sinf*sinf*sinf
    cdef float cos5f = cosf*cosf*cosf*cosf - 10*cosf*cosf*cosf*sinf*sinf +  5*cosf*sinf*sinf*sinf*sinf

    cdef float KPD_lat = 111.13209-0.56605*cos2f+0.0012*cos4f
    cdef float KPD_lon = 111.41513*cosf-0.09455*cos3f+0.00012*cos5f

    cdef float x = (KPD_lon*(lon1-lon2)*180/pi)
    cdef float y = (KPD_lat*(lat1-lat2)*180/pi)
    cdef float rng = sqrt( x*x + y*y ) * 1000.0
    return rng


cpdef set_thunderhour( float lt0, float ln0, uint8[:,:] im, float flashSize ):
    '''
    increments the values in im for all pixels withint flashSize (meters)
    of the point lt0, ln0

    Note, the binSize and bounding box of the im are defined as global constants above
    Deviations from these values are likely to cause errors.

    returns: None
    (computations done in place)
    '''

    cdef int i0,j0,i,j,maxInt
    cdef float binWidth, lt,ln, D

    #get the indices for the thunderhour image which holds this pulse
    i0 = int( (lt0 - BBOX00)/BINSIZE )
    j0 = int( (ln0 - BBOX10)/BINSIZE )

    #note the crude approximation for cosine
    binWidth = cos( lt0*pi/180. ) * 111317.0 * BINSIZE
    # binWidth = ( 1 - (lt0/90)**2 )* 111317.0 * BINSIZE
    maxInt = <int>( flashSize/binWidth ) + 5

    for i in range( i0-maxInt, i0+maxInt+1 ):
        if i < 0 or i >= im.shape[0]: 
            continue
        lt = i*BINSIZE + BBOX00 + BINSIZE/2
        for j in range( j0-maxInt, j0+maxInt+1 ):
            if j < 0 or j >= im.shape[1]: 
                continue
            ln = j*BINSIZE + BBOX10 + BINSIZE/2

            #calculate distance
            D = dist( lt0,ln0, lt,ln )
            if D < flashSize and im[i,j] < 10:
                im[i,j] += 1

