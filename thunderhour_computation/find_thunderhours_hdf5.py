import h5py
import enipy3 as lx
import numpy as np
import sys, time, os
import set_thunderhour

#constants
BINSIZE = 0.05  #this is the resolution of the thunderhour array
BBOX    = [[-90,90],[-180,180]] #this is the bounding box for the thunderhour array
KERNELSIZE = 5  #bins, KERNELSIZE*BINSIZE*R_EARTH > FLASHSIZE
FLASHSIZE  = 5000  #meters
FLASHTYPES = [0,1,40]
OUTPATTERN = 'thunderhours_%Y%m%d_5k.hdf5'
THUNDER_THRESHOLD = 2

def loop_day( tlnData, hdfFile, timeSuffex='UT' ):

    #loop over hours, there are 24 of them in a day
    iTln = 0
    for iHour in range( 24 ):
        print (outPath, iHour)

        #create the thunder hour array
        im = np.zeros( [NLat, NLon], dtype='uint8' )

        #loop over the tln data, which is assumed to be sorted!
        while tlnData.time[iTln] < epoch+(iHour+1)*60*60:
            #we have extra data in the dataset, skip the extraneous stuff
            if tlnData.time[iTln] < epoch+(iHour)*60*60: 
                iTln += 1
                continue
            #flashtype limitations is likely turned off
            if not tlnData.type[iTln] in FLASHTYPES:
                iTln += 1
                continue

            #this is the location of the flash we are looking at right now
            lt = tlnData.lat[iTln]
            ln = tlnData.lon[iTln]

            #sets the appropriate pixels in the image, done in place
            #note, this is a cython library, see set_thunderhour.pyx for implementation
            #done this way for speed
            set_thunderhour.set_thunderhour( lt,ln, im, FLASHSIZE )

            iTln += 1
            #edge case, may not be required
            if iTln >= len( tlnData.time ):
                break

        #convert the array to bools
        im[im<THUNDER_THRESHOLD] = 0
        im[im>=THUNDER_THRESHOLD] = 1
        im = im.astype( 'bool' )
        #save this hour to the hdf5 file
        dset = hdfFile.create_dataset( '%02i%s'%(iHour,timeSuffex), [NLat, NLon], dtype='bool', compression='gzip', data=im )

if __name__ == '__main__':

    try:
        searchDate = sys.argv[-1]
        epoch, nano = lx.timestamp2epochnano( searchDate )
    except:
        print ('find_thunderhours_hdf5 <DATE>')
        sys.exit(1)
    #no matter what the user inputs for a date string, make sure the timestamp
    #references the beginning of the day
    epoch -= epoch%24*60*60

    #we need to load 3 days of data, to deal with the local time thing
    tlnData = None
    for i in range( -1,2 ):
        ###
        # from the date given on the command line, we want to produce 
        # the path to the associate state file, so we can load the data
        archiveTimeTup = time.gmtime( epoch + i*24*3600 )

        #this is the input TLN data archive for the day.  This data is proprietary, and not openly available.
        tlnStatePath = time.strftime( '/data2/Archive/Pulse/%Y/%m/LtgFlashPortions%Y%m%d.state', archiveTimeTup )
        
        ###
        # you just can't trust users these days
        if not os.path.exists( tlnStatePath ):
            raise Exception( 'Input state data does not exist: %s not found'%tlnStatePath )
        
        ###
        # load in the data from the state file which we already tested exists
        if tlnData is None:
            tlnData = lx.Report( tlnStatePath )
        else:
            tlnData.append( lx.Report( tlnStatePath ) )

    ###
    # sort the data by time
    tlnData.truncate( tlnData.time.argsort() )

    ###
    # make the output hdf5 file
    timeTup = time.gmtime( epoch  )
    outPath  = time.strftime( OUTPATTERN, timeTup )
    print ('Creating HDF5 file %s'%outPath)
    hdfFile  = h5py.File( outPath, 'w' )
    # dayGroup = hdfFile.create_group( time.strftime( '%Y-%m-%d', timeTup ) )

    ###
    # we store the thunderhour data in an array, the edges of each 
    # cell are :
    edgesLatitude  = np.arange( BBOX[0][0], BBOX[0][1]+BINSIZE, BINSIZE )
    edgesLongitude = np.arange( BBOX[1][0], BBOX[1][1]+BINSIZE, BINSIZE )
    #this is the size of the thunder hour array
    NLat = len( edgesLatitude )  - 1
    NLon = len( edgesLongitude ) - 1
    
    ###
    # we can set some attribute metadata here
    # print ( 'Setting HDF5 attributes' )
    hdfFile.attrs['date'] = time.strftime( '%Y/%m/%d', timeTup )
    hdfFile.attrs['binsize'] = BINSIZE
    hdfFile.attrs['boundingbox'] = BBOX
    #these edges just barely fit in an attribute
    hdfFile.attrs['edges_latitude'] = edgesLatitude
    hdfFile.attrs['edges_longitude'] = edgesLongitude

    loop_day( tlnData, hdfFile, timeSuffex='UT' )


    ###
    # convert to local time, and do it all again
    timeCorrection = 24*3600*tlnData.lon/360  #in seconds
    tlnData.time += timeCorrection
    ###
    # sort the data by time
    tlnData.truncate( tlnData.time.argsort() )

    ###
    # do the whole loop over again, but in local time this goround
    loop_day( tlnData, hdfFile, timeSuffex='LT' )

    hdfFile.close()



