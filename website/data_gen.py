#!/usr/bin/env python3
import cgi
import os
import re
import time

# f = open( '/var/www/test.log', 'a' )
# f.write('%s\n'%os.getcwd())
# f.write(str(time.time())
# f.close()

print("Content-Type: text/html;charset=utf-8")
print ("Content-type:text/html\r\n")
print( """
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>
        Earth Networks Thunder Hour Repository
    </title>
</head>
<body>""" )

MonthStrings = {
    '01':'Jan',
    '02':'Feb',
    '03':'Mar',
    '04':'Apr',
    '05':'May',
    '06':'Jun',
    '07':'Jul',
    '08':'Aug',
    '09':'Sep',
    '10':'Oct',
    '11':'Nov',
    '12':'Dec'
}

output = ""

#track function:
#// call ../cgi-bin/tracker.py with link // get request from here // want to make this asynchronous so tht browser doesnt hang // do not await
output += "<script> function track(link){  var url = new URL(\"http://thunderhours.earthnetworks.com/cgi-bin/tracker.py\"); var params = [['link', link]]; url.search = new URLSearchParams(params).toString(); fetch(url); } </script>"

dirs = os.listdir("/thunderhours/")
dirs.sort(reverse=True)
ts = time.time()

for year in dirs:
    if (year == "lost+found"):
        continue
    
    #check to make sure the year is really a year (4 numbers)
    if not re.match( r'\d{4}', year ): continue

    #just in case, skip past all files
    directory = "/thunderhours/{}".format(year)
    if not os.path.isdir( directory ): continue
    files = os.listdir(directory)
    files.sort()

    output += "<h1>{}</h1>\n".format(year)
    output += "<table><tr>\n"

    currentMonth = ""
    cellClass = 'even'

    for file in files:
        #is this an hdf5 file?
        if os.path.splitext( file )[1] != '.hdf5': 
            continue

        num = re.search(r'\d{8}', file).group(0)
        month = num[4:6]
        day = num[6:8]
        if month != currentMonth:
            currentMonth = month
            output += " <tr><td class=month>{}: </td>\n".format( MonthStrings[month] )
            if cellClass == 'even':
                cellClass = 'odd'
            else:
                cellClass = 'even'
        output += "   <td class={}><a onclick=\"track(this.href)\" href=\'http://thunderhours.earthnetworks.com/data/{}/{}\'> {} </a></td>\n".format(cellClass,year, file, day)
    output += " </tr>\n</table>\n"
print(output)

print( "</body></html>")

# print("Content-Type: text/html;charset=utf-8")
# print ("Content-type:text/html\r\n")

# output = ""
# dirs = os.listdir("../thunderhours.earthnetworks.com/data")
# dirs.sort(reverse=True)

# for year in dirs:
#     if (year == "lost+found"):
#         continue
#     output += "<h1>"+year+"</h1>"
#     files = os.listdir("../thunderhours.earthnetworks.com/data/"+year)
#     files.sort()

#     for file in files:
#         num = re.search(r'\d{8}', file).group(0)
#         month = num[4:6]
#         day = num[6:8]
#         output += "<a href=\'../data/"+year+"/" + file + "\'>" + year + "/" + month +"/"+ day + "</a><br>\n"
# print(output)


# test if its a directory (is_dir and not lost n found) not dir and not hdf5, dont list.      

