import glob, h5py, sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
"""
Make a plot of the total number of thunderhours for the input files given
"""

###
# Warning, this script relies on Basemap to do the work of creating the world map
# this is a library which is nolonger supported, and really meant for use with 
# python 2.x (which is also no longer supported)
# python 3.x ports of basemap exist, easiest way to get them is through Anaconda

#paths to the files taken from the command line
inPaths = sys.argv[ 1: ]

# bounding box for the result
mapbox = [[-90,90],[-180,180]]


#open the first file to get some metadata from the attributes
#we need the lat/lon edges in this case
f = h5py.File( inPaths[0], 'r' )
lats = f.attrs['edges_latitude']
lons = f.attrs['edges_longitude']
f.close()

#thCounts is the total image
#the lats/lons above are the edges, so we need 1 less value in the image
thCounts = np.zeros( [len(lats)-1, len(lons)-1 ] )
#loop over the input paths and add up the hours
for p in inPaths:
    print ('Reading %s'%p)
    f = h5py.File(p, 'r')
    for i in range(24):
        #there are UT and LT keys for UTC and Local Time
        #we just want to read one of them, it doesn't matter which
        thCounts += f['%02iUT'%i]
    f.close()

#plot making time
fig = plt.figure(figsize=[10,5.05])
plt.subplots_adjust( bottom=0.1, top=0.99, left=0.01, right=0.99)

#this will create the world map
#projection='cyl' is cylindrical projection, which matches the data (square lat/lon boxes)
map = Basemap(projection='cyl',llcrnrlon=mapbox[1][0],llcrnrlat=mapbox[0][0],urcrnrlon=mapbox[1][1],urcrnrlat=mapbox[0][1],resolution='l',area_thresh=10000)
#actually draw some map stuff.  White usually shows up over the default color map
map.drawcoastlines(linewidth=0.25,color='w')
map.drawcountries(linewidth=0.25,color='w')
map.drawstates(linewidth=0.25,color='w')

#convert the lats and lons to map coordinates.  
#because the lats and lons arrays are different lengths, this is done in two steps
x, temp = map(lons,lons)
temp, y = map(lats,lats)
#draw the thunderhour image
cmesh = map.pcolormesh(x,y,thCounts,vmin=0)

#diplay
plt.show()