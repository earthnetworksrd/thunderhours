Repository for things related to the Earth Networks Thunderhours project.
More information at thunderhours.earthnetworks.com

####
# examples
example python scripts which interact with the hdf5 thunderhour data files

- total_thunderhours.py
Makes a map plot of the total number of thunderhours in a set in input 
HDF5 thunderhour archives.

####
# thunderhours_computation
programs included in this directory are here to show the methodology used
to compute the data presented in the thunderhours repository.

- find_thunderhours_hdf5.py
This does the work of actually computing and generating the thunderhour 
hdf5 files included in the thunderhours repository.  
It requires access to proprietary datasets of ENGLN lightning locations
to run, which is not publicly available.  
The code is included though so that the methodology of creating the 
thunderhours maps can be inspected if needed.

- set_thunderhour.pyx
This is a Cython project, which produces C code for use with python.
A compiled binary is included (the .so file), but may not work on all 
platforms.  To build this module, run:
    python3 set_thunderhour_setup.py build_ext --inplace
This will create a new .so file to import into python